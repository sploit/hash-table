//
//  digest.h
//  hashtable
//
//  Created by Victor J. Roemer on 3/6/12.
//  Copyright (c) 2012 Victor J. Roemer. All rights reserved.
//

#ifndef hash_digest_h
#define hash_digest_h

#include <sys/types.h>

unsigned long fnv1a_digest(const void *buf, size_t len, unsigned long hval);
unsigned long sdbm_digest(unsigned char *str);

#endif /* hash_digest_h */
