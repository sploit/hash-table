//
//  digest.c
//  hashtable
//
//  Created by Victor J. Roemer on 3/4/12.
//  Copyright (c) 2012 Victor J. Roemer. All rights reserved.
//

#include <sys/types.h>
#include "digest.h"

// FNV1A digest algorithm from:
// http://isthe.com/chongo/tech/comp/fnv/
//
// Note that this digest works best with hash sizes which are powers of 2.
//
unsigned long fnv1a_digest(const void *buf, size_t len, unsigned long hval)
{
    unsigned char *start = (unsigned char *)buf;
    unsigned char *end = start + len;

    while (start < end) {
        hval ^= (unsigned char)*start++;
        hval *= 0x01000193;
        hval += (hval<<1) + (hval<<4) + (hval<<7) + (hval<<8) + (hval<<24);
    }

    return hval;
}

/* sdbm digest algorithm
 *
 * Use this digest for NULL-terminated strings.
 */
unsigned long sdbm_digest(unsigned char *str)
{
    unsigned long hval = 0;
    int c;

    while (c = *str++)
        hval = c + (hval << 6) + (hval << 16) - hval;

    return hash;
}
